# CLI Arguments : quality ( 1-100 ) default 50
# Example : pythno3 dct.py 50


from math import sqrt, cos, pi, floor
import numpy as np
from PIL import Image
import pickle
import sys
filename = sys.argv[1]
quality = int(sys.argv[2])

img = Image.open(filename)
arr = np.array(img)


# standard Quantization matrix : 50 % compresion
# For other quality compression, new quantization matrix is derived from this standard matrix
quantization_standard = \
    np.array(
        [
            [16, 11, 10, 16, 24, 40, 51, 61],
            [12, 12, 14, 19, 26, 58, 60, 55],
            [14, 13, 16, 24, 40, 57, 69, 56],
            [14, 17, 22, 29, 51, 87, 80, 62],
            [18, 22, 37, 56, 68, 109, 103, 77],
            [24, 35, 55, 64, 81, 104, 113, 92],
            [49, 64, 78, 87, 103, 121, 120, 101],
            [72, 92, 95, 98, 112, 100, 103, 99]
        ]
    )




def get_quantization(q_factor):
    if q_factor < 50:
        s = 5000 / q_factor
    else:
        s = 200 - 2 * q_factor
    r,c = np.shape(quantization_standard)
    
    quantization_matrix = np.zeros((r,c))
    for i in range(r):
        for j in range(c):
            quantization_matrix[i][j] = floor((s * quantization_standard[i][j] + 50) / 100)
            if quantization_matrix[i][j] == 0:
                quantization_matrix[i][j] = 1
    return quantization_matrix



quantization_matrix = get_quantization(quality)


def get_DCT_matrices ( dimension=8 ):
     # t : transform
    matrix1_t = np.zeros(shape=(dimension, dimension))
    matrix1_i = np.zeros(shape=(dimension, dimension))
    
    # matrices for transform_DCT
    for i in range(dimension):
        
        if i == 0:
            c1 = 1/sqrt(2)
        else:
            c1 = 1
        
        for j in range(dimension):
            if j == 0:
                c2 = 1/sqrt(2)
            else:
                c2 = 1
            
            matrix1_t[i][j] = c1*cos((2*j+1)*i*pi/(2*dimension))


    # matrices for inverse_DCT
    for i in range(dimension):
        for j in range(dimension):
            matrix1_i[i][j] = cos((2*i+1)*j*pi/(2*dimension))

    return matrix1_t, matrix1_t.T, matrix1_i, matrix1_i.T



# Matrices for transform_DCT and inverse_DCT
matrix1_t, matrix3_t, matrix1_i, matrix3_i = get_DCT_matrices()


def transform_DCT(pixels, dimension=8):
    return  np.matmul(matrix1_t, np.matmul(pixels, matrix3_t)) * (1/sqrt(2*dimension))


def inverse_DCT(dct_coeff, dimension=8):
    tmp = 1/(sqrt(2))
    tmp_matrix = np.zeros(shape=(dimension, dimension))

    for i in range(dimension):
    
        if i == 0:
            c1 = tmp
        else:
            c1 = 1
        
        for j in range(dimension):
        
            if j == 0:
                c2 = tmp
            else:
                c2 = 1
        
            tmp_matrix[i][j] = c1*c2*dct_coeff[i][j]

    return np.matmul(matrix1_i, np.matmul(tmp_matrix, matrix3_i)) * (1/sqrt(2*dimension))


def quantize (dct_coeff, dimension = 8) :

    result = np.zeros((dimension, dimension))

    for i in range(dimension):
        for j in range(dimension):
            result[i][j] = round ( dct_coeff[i][j] / quantization_matrix[i][j] )

    return result

def dequantize (quantized, dimension = 8) :

    result = np.zeros((dimension, dimension))

    for i in range(dimension):
        for j in range(dimension):
            result[i][j] = quantized[i][j] * quantization_matrix[i][j]

    return result

def rle_zero_reverse(in_arr):
	if in_arr[-1] == 0:
		return in_arr[:-2] + [0] * in_arr[-2]
	else:
		return in_arr

def rle_zero(in_arr):
    count = 0
    for i in in_arr[::-1]:
        if i != 0:
            break
        count += 1
    if count==0:
        return in_arr
    else :
        return in_arr[:len(in_arr) - count] + [count, 0]

def zigzag(in_mat):
    if(in_mat.shape[0]!=in_mat.shape[1]):
        raise "Illegal Matrix:: Needed Square"
        
        
    out = []
    c = 0 
    n = in_mat.shape[0] - 1
    kkop = 2*n +1

    for i in range(0,2*n+1):
        if(i<=n):
            sub = i
        else:
            sub = kkop -i -1
            c = i - n 
            
        if(i%2==0):
            for j in range(sub,-1,-1):
                out.append(in_mat[j+c][sub-j+c])
        else:        
            for j in range(sub,-1,-1):
                out.append(in_mat[sub-j+c][j+c])
    
    return rle_zero(out)
    
    
def invzigzag(in_mat):
    n = 7
    count = 0 
    c = 0 
    out = np.zeros((8,8)).astype(np.int16)
    for i in range(0,16):
        if(i<=n):
            sub = i
        else:
            sub = 14 -i
            c = i - n 
        for j in range(0,sub+1):
            if(i%2==0):
                out[j+c][sub-j+c] = in_mat[count]
            else:
                out[sub-j+c][j+c] = in_mat[count]
            count +=1
    return out.T


def get_bytes( p,q,r,s ):
	p = int(p) + 4096 
	q = int(q) + 4096
	r = int(r) + 4096
	s = int(s) + 4096

	p_str = bin(p)[2:]
	p_str = '0'*(14-len(p_str)) + p_str
	q_str = bin(q)[2:]
	q_str = '0'*(14-len(q_str)) + q_str
	r_str = bin(r)[2:]
	r_str = '0'*(14-len(r_str)) + r_str
	s_str = bin(s)[2:]
	s_str = '0'*(14-len(s_str)) + s_str

	return (int(p_str + q_str + r_str + s_str , 2)).to_bytes(7, byteorder='big')


def jpeg_encode ( pixel ):
    
    pixel = pixel.astype(np.int16) - 127
    dct_coeff = transform_DCT(pixel, 8)
    q = quantize( dct_coeff )    
    zig = zigzag(q)
    return zig

def jpeg_decode ( source ):
    
    a = rle_zero_reverse ( source )
    quanta = invzigzag ( a )
    dq = dequantize ( quanta )
    pixel = inverse_DCT(dq)
    pixel = pixel + 127

    for i in range(pixel.shape[0]):
        for j in range(pixel.shape[1]):
            if pixel[i][j] < 0 :
                pixel[i][j] = 0 
            elif pixel[i][j] > 255:
                pixel[i][j] = 255

    return pixel


def jpeg(source, fileName = filename+str(quality)+'.bin'):
    # compressed image will be stored in it
    width = source.shape[1]
    height = source.shape[0]

    if (height % 8) != 0 or (width % 8) != 0 :
        raise 'width or height is not multiple of 8'

    jpeg_encoded = []
    jpeg_decoded = np.zeros(source.shape)
    for x in range(0, source.shape[0], 8):    
        for y in range(0, source.shape[1], 8):
            tmp = jpeg_encode (source[y:y+8, x:x+8])
            jpeg_encoded.extend(tmp)
            jpeg_decoded[y:y+8, x:x+8] = jpeg_decode (tmp)

    jpeg_decoded = jpeg_decoded.astype(np.uint8)
    
    out = open(fileName, 'wb')

    for i in range(0,len(jpeg_encoded) - (len(jpeg_encoded) % 4 ), 4):

	    out.write( get_bytes( jpeg_encoded[i], jpeg_encoded[i+1], jpeg_encoded[i+2], jpeg_encoded[i+3] ) )
    out.close()

    return jpeg_encoded, jpeg_decoded


# ================================================================  MAIN  ========================================================




if __name__=='__main__':

    data = arr

    jpeg_encoded,jpeg_decoded = jpeg(data)
    print('\n------------------------------------')
    print(data)
    print('\n------------------------------------')
    print(jpeg_decoded)
    print('\n------------------------------------')

    img_compressed = Image.fromarray(jpeg_decoded, 'L')
    img_original = Image.fromarray(data, 'L')

    img_original.show(title = 'original')
    input()
    img_compressed.show( title = 'compressed')
